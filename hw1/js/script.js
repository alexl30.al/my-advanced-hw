"use strict";

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get name() {
    return this._name;
  }

  set name(value) {
    if (value.length <= 1) {
      alert("The Name is too short.");
      return;
    }
    this._name = value;
  }
  get age() {
    return this._age;
  }

  set age(value) {
    if (value >= 120) {
      alert("The people are not lives so long.");
      return;
    }
    this._age = value;
  }
  get salary() {
    return this._salary;
  }

  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary;
  }

  set salary(value) {
    this._salary = value * 3;
  }
}

let employeeParam1 = new Programmer("Petro", 33, 300, "English");
let employeeParam2 = new Programmer("Veronika", 29, 400, "French");
let employeeParam3 = new Programmer("Semen", 39, 600, "Italian");
let employeeParam4 = new Programmer("Iren", 31, 500, "Spanish");

console.log(employeeParam1);
console.log(employeeParam2);
console.log(employeeParam3);
console.log(employeeParam4);
